# Blender Manual Translations

Welcome to the Blender User Manual Translation project!
We are always looking for more translators to help bring Blender's manual to a global audience.

If you're new to translations, it's important to first understand how the English manual is structured.

The [Blender Manual](https://docs.blender.org/manual/en/dev/) is written in `reStructuredText` (RST) and built using
[Sphinx](http://www.sphinx-doc.org/en/stable/). Translations are managed using the **gettext** internationalization system,
which extracts text into `.po` files for translation.

For more information about the user manual, see the [Blender Manual Project](https://projects.blender.org/blender/blender-manual).

## 🌍 How to Translate the Blender Manual
 
To get started, follow the [Translation Contribution Guide](https://docs.blender.org/manual/en/dev/contribute/translate_manual/index.html).

💡 *Want to start translating a new language?*   
If your language isn't listed, open an issue in the [issue tracker](https://projects.blender.org/blender/blender-manual/issues), and we'll set it up for you.

## ⚙️ How to Build the Manual With Translations

1. **[Install](https://docs.blender.org/manual/en/dev/contribute/manual/getting_started/install/index.html)** - Set up the english documentation environment.
2. **[Install Translation Files](https://docs.blender.org/manual/en/dev/contribute/translate_manual/contribute.html#language-files)** - Download the latest translation files for the desired language.
3. **[Build With Translations](https://docs.blender.org/manual/en/dev/contribute/translate_manual/contribute.html#building-with-translations)** - Compile the manual using the downloaded translation files

## 🔗 Useful Links

- **Source Files:** [Translation Repository](https://projects.blender.org/blender/blender-manual-translations)
- **Forum:** [Documentation Category](https://devtalk.blender.org/c/documentation)
- **Chat:** [#translations on chat.blender.org](https://chat.blender.org/#/room/#translations:blender.org)
- **Administrators:** @blendify, @mont29

## 🛠 Reporting Issues

Found a translation issue? Report it [here](https://projects.blender.org/blender/blender-manual/issues/new?template=.gitea%2fissue_template%2fbug.yaml).

---

Thank you for helping make Blender's documentation accessible to more people worldwide! 🚀
