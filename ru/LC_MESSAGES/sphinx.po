# SOME DESCRIPTIVE TITLE.
# Copyright (C) : This page is licensed under a CC-BY-SA 4.0 Int. License
# This file is distributed under the same license as the Blender 2.80 Manual
# package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Blender 2.80 Manual 2.80\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-05 11:37+0900\n"
"PO-Revision-Date: 2025-02-24 16:57+0000\n"
"Last-Translator: Fatih Alexandr <radix83@rambler.ru>\n"
"Language-Team: Russian <https://translate.blender.org/projects/"
"blender-manual/manual-web-ui/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 5.10.1\n"
"Generated-By: Babel 2.10.3\n"

#: ../../build_files/templates/404.html:2
#: ../../build_files/templates/404.html:9
msgid "Not Found (404)"
msgstr "Не найдено (404)"

#: ../../build_files/templates/404.html:10
msgid "It seems the page you are trying to find has been either moved or deleted."
msgstr ""
"Похоже, страница, которую вы пытаетесь найти, была перемещена или удалена."

#: ../../build_files/templates/404.html:11
msgid "You can try one of the following things:"
msgstr "Вы можете попробовать одну из следующих вещей:"

#: ../../build_files/templates/404.html:15
msgid "Search"
msgstr "Поиск"

#: ../../build_files/templates/404.html:27
#: ../../build_files/templates/404.html:28
msgid "Return Home"
msgstr "Вернуться на главную страницу"

#: ../../build_files/templates/components/footer_contribute.html:7
msgid "View Source"
msgstr "Посмотреть источник"

#: ../../build_files/templates/components/footer_contribute.html:11
msgid "View Translation"
msgstr "Посмотреть перевод"

#: ../../build_files/templates/components/footer_contribute.html:23
msgid "Report issue on this page"
msgstr "Сообщить об ошибке на этой странице"

#~ msgid " Not Found (404) "
#~ msgstr "Не найдено (404)"

#~ msgid "Edit Page"
#~ msgstr ""
